import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

public class Folder extends Storable {
	private List<Storable> contents;

	public Folder(String name, List<Storable> contents) {
		super();
		this.name = name;
		this.contents = contents;
	}
	
	public Folder(String name, List<Storable> contents, Folder pre) {
		super();
		this.name = name;
		this.contents = contents;
		this.preFolder = pre;
	}

	public Folder(String name) {
		super();
		this.name = name;
		this.contents = new ArrayList<Storable>();
	}

	private Folder toFolder(Storable s) {
		// TODO Auto-generated constructor stub
		return (Folder)s;
		
	}

	public double height() {
		Stack<Storable> s = new Stack<Storable>();
		s.add(this);
		double total = 0;
		while (!s.isEmpty()) {
			Folder f = (Folder) s.pop();
			if (f.contents == null)
				continue;
			for (int j = 0; j < f.contents.size(); j++) {
				Storable sTmp = f.contents.get(j);
				if (sTmp.getSize() == 0) {
					s.add(sTmp);
				} else {
					total += sTmp.getSize();
				}
			}

		}
		return total;
	}

	public Storable add(Storable s) throws ClassNotFoundException {
		if (!this.equals(s)&&(!isSelfSuccesseurFolder(s))) {
			contents.add(s);
			s.preFolder=this;
			return s;
		}
		else {
			System.out.println("Failed add");
			return this;
		}
	}

	/*
	 * Check if folder is succeseur of this
	 * @params Folder
	 * @return true if is 
	 *         false if is not
	 * 
	 * */
	
	public boolean isSelfSuccesseurFolder(Storable s) throws ClassNotFoundException {
		Folder ss = toFolder(this);
		while(s.getClass().forName("Folder")==this.getClass()&&ss.preFolder!=null) {	
			if(ss.equals(s)) {
				System.err.println(s.name+" is precedent of "+this.name);
				return true;
			}
			ss = ss.preFolder;
		}
		return false;
	}
	
	/*
	 * public Folder preFolder(){ Folder pFolder = new Folder();
	 * while(contents!=null){ pFolder.name = name; pFolder.contents = contents;
	 * contents = contents.next(); } return pFolder; }
	 * 
	 * public void addSameFolder(Storable s){ Folder pFolder = preFolder();
	 * if(preFolder!=null) preFolder.add(s); }
	 */
	@Override
	public double getSize() {
		// TODO Auto-generated method stub
		return 0;
	}

	public static void main(String[] args) {
//		List<Storable> l1 = new ArrayList<Storable>();
//		List<Storable> l2 = new ArrayList<Storable>();
//		List<Storable> l3 = new ArrayList<Storable>();
//		l3.add(new File("file3-1",3.2));
//		l3.add(new Folder("folder3-1"));
//		l2.add(new Folder("folder2-2",l3));
//		l2.add(new File("file2-1",3.2));
//		l2.add(new File("file2-2",4.2));
//		l2.add(new Folder("folder2-1"));
//		l1.add(new Folder("1-1"));
//		l1.add(new File("file1-1", 4.6));
//		l1.add(new Folder("1-2"));
//		l1.add(new File("file1-1", 2.7));
//		l1.add(new Folder("1-3"));
//		l1.add(new File("file1-1", 1.9));
//		l1.add(new Folder("1-4"));
//		l1.add(new File("file1-1", 0.8));
//		l1.add(new Folder("1-5",l2));
//
//		Storable root = new Folder("root", l1);
//		//((Folder) root).add();
//		System.out.println(((Folder) root).height());
		
		/*	root
		 *    |	(l1) file1-* 1-1  1-2 1-3 1-4 1-5
		 *     |	  						(l2)file2-1 file2-2	folder2-1 foler2-2
		 *       |											    (l3)file3-1 folder3-1
		 * 
		 * 
		 * 
		 * 
		 * 
		 * 
		 * 
		 * 
		 * 
		 * 
		 * 
		 * 
		 * 
		 * 
		 * */
		
		Folder root = new Folder("root");
		File f31 = new File("file3-1",3.2);
		Folder fo31 = new Folder("folder3-1");
		Folder fo22 = new Folder("folder2-2");
		File f21 = new File("file2-1",3.2);
		File f22 = new File("file2-2",4.2);
		Folder fo21 = new Folder("folder2-1");
		Folder fo11 = new Folder("1-1");
		File f11 = new File("file1-1", 4.6);
		Folder fo12 = new Folder("1-2");
		File f113 = new File("file1-1", 2.7);
		Folder fo13 = new Folder("1-3");
		File f111 = new File("file1-1", 1.9);
		Folder fo14 = new Folder("1-4");
		File f112 = new File("file1-1", 0.8);
		Folder fo15 = new Folder("1-5");
		try {
			Folder fo11f = (Folder) root.add(fo11);
			fo11f.add(f111);
			Folder fo12f = (Folder) root.add(fo12);
			fo12f.add(f112);
			Folder fo13f = (Folder) root.add(fo13);
			fo13f.add(f113);
			Folder fo14f = (Folder) root.add(fo14);
			fo14f.add(f11);
			Folder fo15f = (Folder) root.add(fo15);
			fo15f.add(f21);
			fo15f.add(f22);
			Folder fo21f = (Folder) fo15f.add(fo21);
			Folder fo22f = (Folder) fo15f.add(fo22);
			Folder fo31f = (Folder) fo21f.add(fo31);
			fo21f.add(f31);
			System.out.println(((Folder) root).height());
			
			fo31f.add(fo21);
			fo31f.add(fo15);
			fo31f.add(fo31);
		
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
