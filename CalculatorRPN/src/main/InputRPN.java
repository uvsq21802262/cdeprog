package main;
import java.util.Scanner;

/**
 * The interface which allows the user to input the operators and operands in the terminal
 */
public class InputRPN {

	/**
	 * Run the calculator interface in which the user inputs the operands and operators.
	 * If the data is not correct, the program asks for another input.
	 * The program ends when the input is "exit".
	 * @throws LackOperandException if there are less than two elements in the stack of operands in the MotorRPN class and an operator is input in the terminal
	 * @throws ArithmeticException if a division by zero is requested
	 */
	public static void startTerminal() throws ArithmeticException {
		Scanner s = new Scanner(System.in);
		MotorRPN motor = new MotorRPN();
		Boolean error = false;

			
		System.out.printf("[*] Welcome to the CalculatorRPN. The range of numbers is [ -%e %e ]\n",Double.MIN_VALUE,
				Double.MAX_VALUE);
		System.out.println("[*] Operation supported: + - * /");
		System.out.println("[*] Exit: exit");
		
		String line = "";
		double input = 0;
		while (!line.toLowerCase().equals("exit")) {
			line = s.nextLine();
			error = false;
			try{
				try {
					input = Double.parseDouble(line);
				} catch (NumberFormatException e) {
					error = true;
					switch (line) {
						case "*":
							motor.operate(Operation.MULT);
							break;
						case "+":
							motor.operate(Operation.PLUS);
							break;
						case "-":
							motor.operate(Operation.MOINS);
							break;
						case "/":
							motor.operate(Operation.DIV);
							break;
						default: 
							System.out.println("Please input a number or an operation");
					}
				}
			}catch(LackOperandException e){
				System.out.println("Not enough operands.");
				error = true;
			}
			if (!error) {
				motor.save(input);
			}
			
			System.out.println("List of the operands : " + motor);
			System.out.print("$ : ");
			//line = s.nextLine();
		}
		
		System.out.println("[*] The program is exited.");
		s.close();
	}
}
