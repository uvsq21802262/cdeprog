package test;

import static org.junit.Assert.*;


import main.LackOperandException;
import main.MotorRPN;
import main.Operation;

import org.junit.Before;
import org.junit.Test;

public class Tests {
	MotorRPN motor;
	@Before
	public void setUp() throws Exception {
		motor = new MotorRPN();
	}
	@Test
	public void calculTests() {
		try {
			motor.save(2);
			motor.save(4);
			assertEquals(8,motor.operate(Operation.MULT),0.1);
			motor.save(2);
			assertEquals(6,motor.operate(Operation.MOINS),0.1);
			motor.save(3);
			assertEquals(2,motor.operate(Operation.DIV),0.1);
			motor.save(1);
			assertEquals(3,motor.operate(Operation.PLUS),0.1);
		} catch (ArithmeticException e) {
		} catch (LackOperandException e) {
		}
	}
	
	
	@Test(expected=LackOperandException.class)
	public void testLackOfOperand() throws ArithmeticException, LackOperandException {
		motor.save(2);
		motor.operate(Operation.MULT);
	}
	
	@Test(expected=ArithmeticException.class)
	public void testDivisionByZero() throws ArithmeticException, LackOperandException {
		motor.save(1);
		motor.save(0);
		motor.operate(Operation.DIV);
	}

}
