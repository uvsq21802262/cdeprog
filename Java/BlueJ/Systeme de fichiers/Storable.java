public abstract class Storable {
	protected Folder preFolder = null;
	protected String name;
	public abstract double getSize();
}
