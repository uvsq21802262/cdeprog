
public class FractionImpl implements Fraction
{
    public static final Fraction ZERO = new FractionImpl(0,1);
    public static final Fraction UN = new FractionImpl(1,1);
    
    private int numerateur;
    private int denominateur;
    
    public FractionImpl(int num, int den){
        this.numerateur = num;
        this.denominateur = den;
    }
    
    public FractionImpl(int number){
        this(number,1);
    }
    
    public FractionImpl(){
        this(0,1);
    }
    
    public int getNumerateur(){
        return numerateur;
    }
    public int getDenominateur(){
        return denominateur;
    }
    public double getNumber(){
        return 1.0*numerateur/denominateur;
    }
    public double addition(Fraction f){
        return (f.getNumerateur()*this.denominateur+f.getDenominateur()*this.numerateur)*1.0/(f.getDenominateur()*this.denominateur);
    }
    public boolean isEqual(Fraction f){
        if(f.getNumber()/this.getNumber()==1) return true;
        return false;
    }
    public String toString(){
        return new String(this.numerateur+"/"+this.denominateur);
    }
    public boolean isBigger(Fraction f){
        return f.getNumber()-this.getNumber()>0?false:true;
    }
}
