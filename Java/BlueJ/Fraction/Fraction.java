public interface Fraction
{
    /*public static final Fraction ZERO = new Fraction(0,1);
    public static final Fraction UN = new Fraction(1,1);
    */
    public int getNumerateur();
    public int getDenominateur();
    public double getNumber();
    public double addition(Fraction f);
    public boolean isEqual(Fraction f);
    public String toString();
    public boolean isBigger(Fraction f);
}
